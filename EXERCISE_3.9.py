from search import *

class MissionariesAndCannibals(Problem):
    def value(self, state):
        pass

    def __init__(self):
        # Initial state is (3, 3, 1) as depicted on state diagram
        # Likewise, goal is (0, 0, 0).
        initial, goal = (3, 3, 1), (0, 0, 0)
        super().__init__(initial, goal)

    def actions(self, state):
        # Set of all possible actions in problem
        possible_actions = ['m', 'c', 'mm', 'cc', 'mc']

        if (state not in [
            (2, 2, 0),
            (3, 2, 1),
            (0, 2, 0),
            (2, 2, 1),
            (0, 1, 0),
            (1, 1, 1)
        ]):
            possible_actions.remove('m')

        if (state not in [
            (3, 3, 1),
            (3, 2, 0),
            (3, 1, 0),
            (3, 2, 1),
            (3, 0, 0),
            (3, 1, 1),
            (0, 2, 0),
            (0, 3, 1),
            (0, 1, 0),
            (0, 2, 1)
        ]):
            possible_actions.remove('c')

        if (state not in [
            (2, 2, 0),
            (3, 3, 1),
            (2, 2, 1),
            (1, 1, 0),
            (1, 1, 1),
            (0, 0, 0)
        ]):
            possible_actions.remove('mc')

        if(state not in [
            (1, 1, 0),
            (3, 1, 1)
        ]):
            possible_actions.remove('mm')

        if(state not in [
            (3, 3, 1),
            (3, 1, 0),
            (3, 2, 1),
            (3, 0, 0),
            (0, 3, 1),
            (0, 1, 0),
            (0, 2, 1),
            (0, 0, 0)
        ]):
            possible_actions.remove('cc')

        print("Current state is: {}; possible actions are: {}".format(state, possible_actions))
        return possible_actions

    def result(self, state, action):
        new_state = state

        if (action == 'm'):
            if (state == (2, 2, 0)):
                new_state = (3, 2, 1)
            elif (state == (3, 2, 1)):
                new_state = (2, 2, 0)
            elif(state == (2, 2, 1)):
                new_state = (0, 2, 0)
            elif (state == (0, 2, 0)):
                new_state = (2, 2, 1)
            elif(state == (0, 1, 0)):
                new_state = (1, 1, 1)
            elif (state == (1, 1, 1)):
                new_state = (0, 1, 0)

        elif (action == 'c'):
            if (state == (3, 3, 1)):
                new_state = (3, 2, 0)
            elif (state == (3, 2, 0)):
                new_state = (3, 3, 1)
            elif (state == (3, 1, 0)):
                new_state = (3, 2, 1)
            elif (state == (3, 2, 1)):
                new_state = (3, 1, 0)
            elif (state == (3, 0, 0)):
                new_state = (3, 1, 1)
            elif (state == (3, 1, 1)):
                new_state = (3, 0, 0)
            elif (state == (0, 2, 0)):
                new_state = (0, 3, 1)
            elif (state == (0, 3, 1)):
                new_state = (0, 2, 0)
            elif (state == (0, 1, 0)):
                new_state = (0, 2, 1)
            elif (state == (0, 2, 1)):
                new_state = (0, 1, 0)

        elif (action == 'mc'):
            if (state == (2, 2, 0)):
                new_state = (3, 3, 1)
            elif (state == (3, 3, 1)):
                new_state = (2, 2, 0)
            elif (state == (2, 2, 1)):
                new_state = (1, 1, 0)
            elif (state == (1, 1, 0)):
                new_state = (2, 2, 1)
            elif (state == (1, 1, 1)):
                new_state = (0, 0, 0)
            elif (state == (0, 0, 0)):
                new_state = (1, 1, 1)

        elif (action == 'mm'):
            if (state == (3, 1, 1)):
                new_state = (1, 1, 0)
            elif (state == (1, 1, 0)):
                new_state = (3, 1, 1)

        elif (action == 'cc'):
            if (state == (3, 3, 1)):
                new_state = (3, 1, 0)
            elif (state == (3, 1, 0)):
                new_state = (3, 3, 1)
            elif (state == (3, 2, 1)):
                new_state = (3, 0, 0)
            elif (state == (3, 0, 0)):
                new_state = (3, 2, 1)
            elif state == (0, 3, 1):
                new_state = (0, 1, 0)
            elif state == (0, 1, 0):
                new_state = (0, 3, 1)
            elif state == (0, 2, 1):
                new_state = (0, 0, 0)
            elif state == (0, 0, 0):
                new_state = (0, 2, 1)

        return new_state

    def goal_test(self, state):
        goalReached = super().goal_test(state)

        if goalReached == True:
            print('Goal is reached! Current state: {}'.format(state))

        return goalReached


# Initialize problem
problem = MissionariesAndCannibals()

# Most optimal from tested methods
depth_first_graph_search(problem)

# Least optimal as it produces an infinite cycle
# depth_first_tree_search(problem)

# Slightly less optimal than depth_first_graph
# breadth_first_graph_search(problem)

# Solves the problem but inefficient space-wise
# breadth_first_tree_search(problem)

# Similar performance to breadth_first_graph_search
# uniform_cost_search(problem, False)

# Solves the problem but non-optimally space-wise.
# iterative_deepening_search(problem)