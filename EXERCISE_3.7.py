from search import *

class PolygonProblem(Problem):
    def __init__(self):
        pass

    def actions(self, state):
        pass

    def result(self, state, action):
        pass


# Heuristic function to calculate the straight line distance
# between the current state n and the goal state.
def h(n):
    goal = (0, 0)
    X = pow((n[0]-goal[0]), 2)
    Y = pow((n[1]-goal[1]), 2)

    return pow((X+Y), 0.5)